# [kadi-run](https://gitlab.com/humin-game-lab/kadi/kadi-run)
These are specific commands you can register for the ability that can be be called directly from the cli.

### [`kadi run [key] [-a abilityName]`](https://gitlab.com/humin-game-lab/kadi/kadi-run)

Executes a custom command specified under the `run` object in the `agent.json` file. The `[key]` parameter corresponds to the action name defined in `run`.  If no `key` is defined, then it will attempt to execute the `start` command within the `agent.json` file. `[key]` can also be `init` or `setup` to run default scripts asscoiated with those fields in agent.json.

Scripts from abilities can also be run by using the `-a` (or `--a`, `-ability`, `--ability`) flag, followed by the name of the ability.  This will run the script associated with the `key` (or the `start` script if no `key` is provided) from the named ability


**Examples:**

*    To execute the `greet` action defined in the `run` property within the `agent.json` :

`kadi run greet`

*    To run the default `start` command listed in the `agent.json`

`kadi run`
or
`kadi run start`

*   To run the default `start` command listed in an ability, either of teh following would work

`kadi run -a myAbility`
or
`kadi run start -a myAbility`

Roadmap Features
---------

*  ~~Add features to use an ability flag `-a`~~
    *   ~~value after the flag will be the name of an ability, and the associated command passed will be run from the associated ability folder~~
    *  ~~Function will need to grab agent.json for project, and identify the current version installed for the ability name~~
        *  ~~If not located in the agent.json return error~~
        *  ~~If folder not found for the ability listed in agent.json return error~~
    * ~~runCommand will then be called from the ability, version path~~
import {getKadiJSON, getAbilityJSON, getProjectJSON, runSpawnCommand, findAbilityVersionByName} from 'kadi-core';

// Get the API URL from the agent.json
let kadijson = getKadiJSON();

// Define the API URL (Adjust according to your API)
const SEARCH_API_URL    = kadijson.api + '/search';
const GET_API_URL       = kadijson.api + "/get";

//Get flag value
function getFlagValue(flags, args){
    // Iterate over args to find the first occurrence of any of the specified flags
    for (let i = 0; i < args.length - 1; i++) { // Loop to the second-last element to ensure there's a next element
        if (flags.includes(args[i])) {
            // Return the next element immediately after the flag
            return args[i + 1];
        }
    }
    return null; // Return null if no flag is found or if it's the last element in the array
}
// Takes command line args and splits out the flags and their vlaues
function splitArgsAndFlags(inputArray) {
    // Find the index of the first flag (element starting with '-')
    const flagIndex = inputArray.findIndex(element => element.startsWith('-'));

    if (flagIndex === -1) {
        // No flags found, return the whole array and an empty flags array
        return {
            cliargs: inputArray[0],
            cliflags: []
        };
    } else {
        // Split the array into before and after the flag
        const cliargs = flagIndex == 0 ? null : (inputArray.slice(0, flagIndex))[0];
        const cliflags = inputArray.slice(flagIndex);
        return {
            cliargs,
            cliflags
        };
    }
}
//Entry point for module, installs abilities from agent.json
export default async function(args) {
    const [script, ...flags] = args;

    var { cliargs, cliflags } = splitArgsAndFlags(args);
    // console.log("cliargs: ", cliargs, " cliflags: ", cliflags);

    const projectJSON = await getProjectJSON();

    const abilityName = getFlagValue(['-a', '--a', '-ability', '--ability'], cliflags);

    if (abilityName) { //Contains ability flag, run from abilities directory

        //Get version of ability from agent.json
        const abilityVersion = findAbilityVersionByName(projectJSON.abilities, abilityName);

        if( abilityName && abilityVersion){
            //Get the ability JSON
            const abilityJSON = getAbilityJSON(abilityName, abilityVersion);
            // console.log("abilityJSON: ", abilityJSON);

            if(!cliargs){ //No script provided, run default start command
                if(abilityJSON.start){
                    await runSpawnCommand(abilityName, abilityVersion, abilityJSON.start);
                }
                else{
                    console.log("No default Start command found in agent.json")
                }
            }
            else if(cliargs === "init"){ //Run the init script provided
                if(abilityJSON.init){
                    await runSpawnCommand(abilityName, abilityVersion, abilityJSON.init);
                }
                else{
                    console.log("No default Init command found in agent.json")
                }
            }
            else if(cliargs === "setup"){ //Run the setup script provided
                if(abilityJSON.setup){
                    await runSpawnCommand(abilityName, abilityVersion, abilityJSON.setup);
                }
                else{
                    console.log("No default Setup command found in agent.json")
                }
            }
            else if(cliargs === "start"){ //Run the setup script provided
                if(abilityJSON.start){
                    await runSpawnCommand(abilityName, abilityVersion, abilityJSON.start);
                }
                else{
                    console.log("No default Start command found in agent.json")
                }
            }
            else{ //Run the script provided
                if (abilityJSON.run && abilityJSON.run[cliargs]) {
                    await runSpawnCommand(abilityName, abilityVersion, abilityJSON.run[cliargs]);
                } else {
                    console.log(`No command found for key: ${cliargs}`);
                }
            }
        }

    } else { //No abilities flag, run from project root
        if(!cliargs){ //No script provided, run default start command
            if(projectJSON.start){
                await runSpawnCommand("","",projectJSON.start);
            }
            else{
                console.log("No default Start command found in agent.json")
            }
        }
        else if(cliargs === "init"){ //Run the init script provided
            if(projectJSON.init){
                await runSpawnCommand("","",projectJSON.init);
            }
            else{
                console.log("No default Init command found in agent.json")
            }
        }
        else if(cliargs === "setup"){ //Run the setup script provided
            if(projectJSON.setup){
                await runSpawnCommand("","",projectJSON.setup);
            }
            else{
                console.log("No default Setup command found in agent.json")
            }
        }
        else if(cliargs === "start"){ //Run the setup script provided
            if(projectJSON.start){
                await runSpawnCommand("","",projectJSON.start);
            }
            else{
                console.log("No default Start command found in agent.json")
            }
        }
        else{ //Run the script provided
        
            if (projectJSON.run && projectJSON.run[cliargs]) {
                await runSpawnCommand("","",projectJSON.run[cliargs]);
            } else {
                console.log(`No command found for key: ${cliargs}`);
            }
        }
    }

    
};
